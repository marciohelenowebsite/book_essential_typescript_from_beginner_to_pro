# Book_Essential_TypeScript_From_Beginner_to_Pro

Estudo do TypeScript

**Links importantes**

- [Inquirer](https://github.com/SBoudrias/Inquirer.js)
  Uma coleção de interfaces de usuário de linha de comando interativas comuns.

- [DefinitelyTyped](https://github.com/DefinitelyTyped/DefinitelyTyped)
  O repositório para definições de tipo TypeScript de alta qualidade.

- [Lowdb](https://github.com/typicode/lowdb)
  Banco de dados JSON pequeno para Node, Electron e o navegador. Alimentado por Lodash.
