"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TodoItem {
    constructor(id, tarefa, completo = false) {
        this.id = id;
        this.tarefa = tarefa;
        this.completo = completo;
    }
    /**
     * imprimeDetalhe
     */
    imprimeDetalhe() {
        console.log(`${this.id}\t${this.tarefa} ${this.completo ? '\t(completo)' : ''}`);
    }
}
exports.TodoItem = TodoItem;
