"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TodoItem_1 = require("./TodoItem");
class TodoCollection {
    constructor(userName, todoItems = []) {
        this.userName = userName;
        this.todoItems = todoItems;
        this.nextId = 1;
        this.itemMap = new Map();
        todoItems.forEach(item => this.itemMap.set(item.id, item));
    }
    addTodo(task) {
        while (this.getTodoById(this.nextId)) {
            this.nextId++;
        }
        // this.todoItems.push(new TodoItem(this.nextId, task));
        this.itemMap.set(this.nextId, new TodoItem_1.TodoItem(this.nextId, task));
        return this.nextId;
    }
    getTodoById(id) {
        return this.itemMap.get(id);
    }
    /**
     * getTodoItems
     */
    getTodoItems(includeComplete) {
        return [...this.itemMap.values()].filter(item => includeComplete || !item.completo);
    }
    markComplete(id, completo) {
        const todoItem = this.getTodoById(id);
        if (todoItem) {
            todoItem.completo = completo;
        }
    }
    /**
     * removeComplete
     */
    removeComplete() {
        this.itemMap.forEach(item => {
            if (item.completo) {
                this.itemMap.delete(item.id);
            }
        });
    }
    /**
     * getItemCounts
     */
    getItemCounts() {
        return {
            total: this.itemMap.size,
            incompleto: this.getTodoItems(false).length
        };
    }
}
exports.TodoCollection = TodoCollection;
