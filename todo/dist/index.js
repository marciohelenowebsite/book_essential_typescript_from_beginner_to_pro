"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const TodoItem_1 = require("./TodoItem");
const inquire = __importStar(require("inquirer"));
const jsonTodoCollection_1 = require("./jsonTodoCollection");
let todos = [
    new TodoItem_1.TodoItem(1, "Buy Flowers"),
    new TodoItem_1.TodoItem(2, "Get Shoots"),
    new TodoItem_1.TodoItem(3, "Collect Tickets", true),
    new TodoItem_1.TodoItem(4, "Call Joe", true)
]; // array 4 TodoItem
let collection = new jsonTodoCollection_1.JsonTodoCollection("Adam", todos); // variable collection 
let showCompleted = true;
function displayTodoList() {
    console.log(`${collection.userName}'s Todo List` + `(${collection.getItemCounts().incompleto} items to do)`);
    collection.getTodoItems(showCompleted).forEach(item => item.imprimeDetalhe());
}
var Commands;
(function (Commands) {
    Commands["Add"] = "Add new Task";
    Commands["Complete"] = "Complete Task";
    Commands["Toggle"] = "Mostrar/oculto conclu\u00EDdo";
    Commands["Purge"] = "Remove Completed Task";
    Commands["Quit"] = "Quit";
})(Commands || (Commands = {}));
function promptAddTask() {
    console.clear();
    inquire.prompt({ type: 'input', name: 'add', message: 'Enter Taks: ' })
        .then(answers => {
        if (answers["add"] !== "") {
            collection.addTodo(answers["add"]);
        }
        promptUser();
    });
}
function promptComplete() {
    console.clear();
    inquire.prompt({
        type: 'checkbox',
        name: 'complete',
        message: 'Mark Tasks Complete',
        choices: collection.getTodoItems(showCompleted)
            .map(item => ({
            name: item.tarefa, value: item.id, checked: item.completo
        }))
    }).then(answers => {
        let completedTask = answers["complete"];
        collection.getTodoItems(true).forEach(item => {
            collection.markComplete(item.id, completedTask.find(id => id === item.id) != undefined);
        });
        promptUser();
    });
}
function promptUser() {
    console.clear();
    displayTodoList();
    inquire.prompt({
        type: "list",
        name: "command",
        message: "Choose option",
        choices: Object.values(Commands)
    }).then(answers => {
        switch (answers["command"]) {
            case Commands.Toggle:
                showCompleted = !showCompleted;
                promptUser();
                break;
            case Commands.Add:
                promptAddTask();
                break;
            case Commands.Complete:
                if (collection.getItemCounts().incompleto > 0) {
                    promptComplete();
                }
                else {
                    promptUser();
                }
                break;
            case Commands.Purge:
                collection.removeComplete();
                promptUser();
                break;
        }
    });
}
promptUser();
