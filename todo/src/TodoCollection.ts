import { TodoItem } from './TodoItem'

type ItemCounts = {
  total: number,
  incompleto: number
}

export class TodoCollection {
  private nextId: number = 1;
  protected itemMap = new Map<number, TodoItem>();

  constructor(public userName: string, public todoItems: TodoItem[] = []) {
    todoItems.forEach(item => this.itemMap.set(item.id, item))
  }

  public addTodo(task: string): number {
    while (this.getTodoById(this.nextId)) {
      this.nextId++;
    }
    // this.todoItems.push(new TodoItem(this.nextId, task));
    this.itemMap.set(this.nextId, new TodoItem(this.nextId, task))
    return this.nextId;
  }

  public getTodoById(id: number): TodoItem | undefined {
    return this.itemMap.get(id)
  }

  /**
   * getTodoItems
   */
  public getTodoItems(includeComplete: boolean): TodoItem[] {
    return [...this.itemMap.values()].filter(item => includeComplete || !item.completo);
  }

  public markComplete(id: number, completo: boolean) {
    const todoItem = this.getTodoById(id);
    if (todoItem) {
      todoItem.completo = completo;
    }
  }

  /**
   * removeComplete
   */
  public removeComplete() {
    this.itemMap.forEach(item => {
      if (item.completo) {
        this.itemMap.delete(item.id)
      }
    })
  }

  /**
   * getItemCounts
   */
  public getItemCounts(): ItemCounts {
    return {
      total: this.itemMap.size,
      incompleto: this.getTodoItems(false).length
    }
  }

}