import { TodoItem } from './TodoItem'
import { TodoCollection } from './TodoCollection'
import * as inquire from 'inquirer';
import { JsonTodoCollection } from './jsonTodoCollection'

let todos: TodoItem[] = [
  new TodoItem(1, "Buy Flowers"),
  new TodoItem(2, "Get Shoots"),
  new TodoItem(3, "Collect Tickets", true),
  new TodoItem(4, "Call Joe", true)
] // array 4 TodoItem

let collection: TodoCollection = new JsonTodoCollection("Adam", todos); // variable collection 
let showCompleted = true

function displayTodoList(): void {
  console.log(`${collection.userName}'s Todo List` + `(${collection.getItemCounts().incompleto} items to do)`);
  collection.getTodoItems(showCompleted).forEach(item => item.imprimeDetalhe());
}

enum Commands {
  Add = "Add new Task",
  Complete = "Complete Task",
  Toggle = "Mostrar/oculto concluído",
  Purge = "Remove Completed Task",
  Quit = "Quit"
}

function promptAddTask(): void {
  console.clear()
  inquire.prompt({ type: 'input', name: 'add', message: 'Enter Taks: ' })
    .then(answers => {
      if (answers["add"] !== "") {
        collection.addTodo(answers["add"])
      }
      promptUser()
    })
}

function promptComplete(): void {
  console.clear()
  inquire.prompt({
    type: 'checkbox',
    name: 'complete',
    message: 'Mark Tasks Complete',
    choices: collection.getTodoItems(showCompleted)
      .map(item => ({
        name: item.tarefa, value: item.id, checked: item.completo
      }))

  }).then(answers => {
    let completedTask = answers["complete"] as number[]
    collection.getTodoItems(true).forEach(item => {
      collection.markComplete(item.id, completedTask.find(id => id === item.id) != undefined)
    });
    promptUser()
  })

}

function promptUser(): void {
  console.clear();
  displayTodoList();
  inquire.prompt({
    type: "list",
    name: "command",
    message: "Choose option",
    choices: Object.values(Commands)
  }).then(answers => {
    switch (answers["command"]) {
      case Commands.Toggle:
        showCompleted = !showCompleted
        promptUser();
        break;
      case Commands.Add:
        promptAddTask()
        break;
      case Commands.Complete:
        if (collection.getItemCounts().incompleto > 0) {
          promptComplete()
        } else {
          promptUser()
        }
        break;
      case Commands.Purge:
        collection.removeComplete()
        promptUser()
        break;
    }
  })
}

promptUser();
