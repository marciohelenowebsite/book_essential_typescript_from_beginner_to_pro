export class TodoItem {
  constructor(
    public id: number,
    public tarefa: string,
    public completo: boolean = false,
  ) { }

  /**
   * imprimeDetalhe
   */
  public imprimeDetalhe(): void {
    console.log(
      `${this.id}\t${this.tarefa} ${this.completo ? '\t(completo)' : ''}`,
    );
  }
}
